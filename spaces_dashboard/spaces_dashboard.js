if (typeof(Drupal) == "undefined" || !Drupal.spaces_dashboard) {
  Drupal.spaces_dashboard = {};
}

Drupal.spaces_dashboard.setstate = function() {
  if ($('form#spaces-dashboard-users input[@checked]').size() > 0 && $('form#spaces-dashboard-users select#edit-groups').val() != 0) {
    $('form#spaces-dashboard-users select#edit-actions').attr('disabled', false);
  }
  else {
    $('form#spaces-dashboard-users select#edit-actions').attr('disabled', true);
  }
}

Drupal.spaces_dashboard.attach = function () {
  $('form#spaces-dashboard-users select#edit-groups').change(function() {
    var gid = $(this).val();
    if (gid == 0) {
      $('form#spaces-dashboard-users table tr').removeClass('lit');
    }
    else {
      $('form#spaces-dashboard-users table tr').removeClass('lit');
      $('form#spaces-dashboard-users table tr.og-'+gid).addClass('lit');
    }
    Drupal.spaces_dashboard.setstate();
  });
  $('form#spaces-dashboard-users input.form-checkbox').change(function() {
    Drupal.spaces_dashboard.setstate();
  });
}
	
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    if ($('form#spaces-dashboard-users').size() > 0) {
      Drupal.spaces_dashboard.attach();
    }
  });
};
