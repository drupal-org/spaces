if (typeof(Drupal) == "undefined" || !Drupal.shout) {    
  Drupal.shout = {};
}

Drupal.shout.attach = function () {
  $("form#spaces-shoutbox-ajaxform").submit(function() {
    var shout = $('input.form-text', this).val();
    if (shout.length > 0) {
      $.get(
        Drupal.shout.shout_url,
        {'shout': shout},
        function(data) {
          if (data) {
            $('form#spaces-shoutbox-ajaxform input.form-text').val('');
            $('div#spaces-shouts').prepend(data);
            return false;
          }
        }
      );
    }
    return false;
  });
}
	
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    if ($('form#spaces-shoutbox-ajaxform').size() > 0) {
      Drupal.shout.attach();
    }
  });
};
