<?php

/**
 * Implementation of hook_context_define().
 */
function spaces_shoutbox_context_define() {
  $items = array();
  $items[] = array(
    'namespace' => 'spaces',
    'attribute' => 'feature',
    'value' => 'shoutbox',
    'spaces' => array(
      'label' => t('Shoutbox'),
      'description' => t('A shoutbox for informal message blasts.'),
      'types' => array('og'),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info()
 */
function spaces_shoutbox_node_info() {
  return array(
    'shout' => array(
      'name'        => t('Shout'),
      'module'      => 'spaces_shoutbox',
      'description' => t('A shoutbox shout.'),
      'locked'      => true,
    )
  );
}

/**
 * Implementation of hook_form()
 */
function spaces_shoutbox_form(&$node) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
    '#required' => true,
    '#default_value' => $node->title,
  );
  return $form;
}

/**
 * Implementation of hook_menu()
 */
function spaces_shoutbox_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'js/shoutbox',
      'description' => t('Shoutbox AJAX submission callback.'),
      'callback' => 'spaces_shoutbox_ajax',
      'access' => user_access('access content'),
      'type' => MENU_CALLBACK,
    );
  }
  return $items;
}

/**
 * Shoutbox AJAX callback
 */
function spaces_shoutbox_ajax() {
  if (isset($_GET['shout']) && $space = spaces_get_space()) {
    if (check_plain($_GET['shout']) && !empty($_GET['shout'])) {
      global $user;
      $node = new stdClass();
      $node->uid  = $user->uid;
      $node->title = $_GET['shout'];
      $node->type  = 'shout';
      $node->status= 1;
      
      if ($gid = $space->sid) {
        $node->og_groups = array($gid);
        $node->og_public = 0;
      }

      node_validate($node);
      node_save($node);
      print theme('spaces_shout', array('name' => theme('username', $user), 'title' => $node->title));
      exit;
    }
  }
  exit;
}

/**
 * Implementation of hook_block()
 */
function spaces_shoutbox_block($op = 'list', $delta = 0) {
  if ($op == 'list') {
    $blocks[1] = array(
      'info' =>  t('Spaces: Shoutbox'),
      'region' => 'right',
      'status' => 1,
    );
    return $blocks;
  }
  else if ($op == 'view') {
    switch ($delta) {
      case 1:
        // If this isn't a groups site, or if it is and the shoutbox feature is enabled...
        $space = spaces_get_space();
        if ($space && ($space->features['shoutbox'] != SPACES_FEATURE_DISABLED) && $space->feature_access('shoutbox')) {
          drupal_add_css(drupal_get_path('module', 'spaces_shoutbox') .'/spaces_shoutbox.css');
          drupal_add_js(drupal_get_path('module', 'spaces_shoutbox') .'/spaces_shoutbox.js');

          $shoutbox_url = url('js/shoutbox');
          $js = "Drupal.extend({shout: {shout_url:'$shoutbox_url'}})";
          drupal_add_js($js, 'inline');

          $view = views_get_view('spaces_shouts');
          $block['content'] = "<div id='spaces-shouts'>". views_build_view('block', $view, array(), false, $view->nodes_per_block) ."</div>";
          $block['content'] .= drupal_get_form('spaces_shoutbox_ajaxform');
          $block['subject'] = t('Shoutbox');
          return $block;
        }
        break;
    }
  }
}

/**
 * Shoutbox form definition
 */
function spaces_shoutbox_ajaxform() {
  $form = array(
    '#action' => null,
    'shout' => array(
      '#type'     => 'textfield',
      '#size'     => 20,
      '#required' => true,      
    ),
    'submit' => array(
      '#type'   => 'button',
      '#value'  => t('Shout'),
    ),
  );
  $form['#theme'] = ''; // force default renderer
  return $form;
}

/**
 *  Shoutbox markup
 */
function theme_spaces_shout($vars) {
  extract($vars);
  $title = _filter_url($title, 'shout'); // link shout urls
  return "<div class='shout'><span class='name'>$name</span> $title</div>";
}

/**
 *  Custom spaces shout theme f()
 */
function theme_views_view_list_spaces_shouts($view, $nodes, $type) {
  $fields = _views_get_fields();
  $items = array();
  foreach ($nodes as $node) {
    $item = array();
    foreach ($view->field as $field) {
      if (!isset($fields[$field['id']]['visible']) && $fields[$field['id']]['visible'] !== FALSE) {
        $item[$field['field']] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
      }
    }
    $items[] = theme('spaces_shout', $item);
  }
  return implode("\n", $items);
}

/**
 *  Implementation hook_views_default_views()
 */
function spaces_shoutbox_views_default_views() {
  $views = array();
  $view = _spaces_shoutbox_views_shouts();
  $views[$view->name] = $view;
  return $views;
}

function _spaces_shoutbox_views_shouts() {
  $view = new stdClass();
  $view->name = 'spaces_shouts';
  $view->description = t('Displays a short list of shouts.');
  $view->access = array();
  $view->view_args_php = '';
  $view->block = TRUE;
  $view->block_title = t('Shoutbox');
  $view->block_empty = '<p class="views-empty">'.t('There are no shouts to view.').'</p>';
  $view->block_empty_format = '1';
  $view->block_type = 'list';
  $view->nodes_per_block = '10';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array();
  $view->field = array (
    array (
      'tablename' => 'users',
      'field' => 'name',
      'label' => '',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'nolink',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
        0 => 'shout',
      ),
    ),
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'og_ancestry',
      'field' => 'picg',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_GID***',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'operator' => '>',
      'options' => -1*SPACES_ARCHIVE_TIMESTAMP,
      'value' => 'now',
    ),
  );
  $view->exposed_filter = array();
  $view->requires = array(node, users);
  return $view;
}
